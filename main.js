currentState = {
    "score": 0,
    "state": "wait",
};

const CATEGORY_ID_MAP = {
    'Animals' : '27',
    'Sports' : '21',
    'General Knowledge' : '9',
    'Random' : null
};

var questions;
var correctAnswer;
var points = 0;
var pointsPossible;


function startGame() {
    pointsPossible = questions.length;
    nextTurn();
}

function submitAnswer(element) {
    if (currentState.state == 'wait')
        return;
    currentState.state = 'wait';
    answer = element.innerHTML;
    console.log(answer + correctAnswer);
    if (answer === correctAnswer) {
        points ++;
        toast("Correct!");
    }
    else {
        toasterMessage = '<p>Wrong!</p>' +
            '<p style="font-size:.7em;">Correct answer is: ' + correctAnswer + '</p>';
        toast(toasterMessage);
    }
    answerButtons = document.getElementsByClassName('answer-button');
    for(i = 0; i < answerButtons.length; ++i) {
        btn = answerButtons[i];
       if (btn.innerHTML === correctAnswer) {
           btn.style.color = '#01d72e';
           btn.style.borderColor = '#01d72e';
       }
       else if (btn.innerHTML === answer) {
           btn.style.color = '#ff1a00';
           btn.style.borderColor = '#ff1a00';
       }
    }

    html = '<button class="button-next" onclick="nextTurn()">Next Question</button>';
    document.getElementById('control-panel').innerHTML = html;
}

function nextTurn() {
    if (questions.length == 0) {
        endGame();
        return;
    }
    currentState.state = 'play';
    document.getElementById('control-panel').innerHTML = '';
    question = questions[0];
    questions = questions.slice(1);
    correctAnswer = question['correct_answer'];
    answers = question['incorrect_answers'];
    answers.push(correctAnswer);
    answers = randomize(answers);
    toast(question['question']);
    html = '';
    for (i = 0; i < answers.length; ++i) {
        html += '<button class="answer-button button-start" onclick="submitAnswer(this)">' + answers[i] + '</button>';
    }
    document.getElementById('game-board').innerHTML = html;
}

function endGame() {
    document.getElementById('control-panel').innerHTML = '';
    html = '<button class="button-start" onclick="resetGame()">New Game</button>'
    document.getElementById('game-board').innerHTML = html;
    toast('<p>Game Over!</p>' + points + "/" + pointsPossible + " Correct");

}

function randomize(answers) {
    l = answers.length;
    n = Math.floor(Math.random() * l);
    randomizedAnswers = [];
    for (i = 0; i < l; ++i) {
        randomizedAnswers[i] = answers[n++ % l]
    }
    return randomizedAnswers;
}

function resetGame(){
    toast('Select A Category');
    points = 0;
    document.getElementById('game-board').innerHTML = ' <button class="button-start category-button">Animals</button>\n' +
        '        <button class="button-start category-button">Sports</button>\n' +
        '        <button class="button-start category-button">General Knowledge</button>\n' +
        '        <button class="button-start category-button">Random Category</button>';
}


function toast(message) {
    document.getElementById('toaster').innerHTML = message;
}

$(document).ready(function(){
    $(".game-board").on('click', '.category-button', function() {
        category = $(this).text();
        console.log(category);
        categoryId = CATEGORY_ID_MAP[category];
        console.log(categoryId);
        apiUrl = 'https://opentdb.com/api.php?amount=5&type=multiple';
        $.ajax({
            url: categoryId ? apiUrl + '&category=' + categoryId : apiUrl,
            crossDomain: true,
            dataType: "json",
            success: function (parsed_json) {
                console.log(parsed_json);
                questions = parsed_json['results'];
                currentState.state = "play";
                startGame();
            }
        });
    })
});
